console.log("App.js is running!");

// JSX - Javascript XML
const app = {
  title: 'Some Title',
  subtitle: 'This is some info',
  options: []
}

const onFormSubmit = (e) => {
  e.preventDefault();

  const option = e.target.elements.option.value;

  if (option) {
    app.options.push(option);
    e.target.elements.option.value = '';
    renderApp();
  }
}

const remove = () => {
  app.options = [];
  renderApp();
}

const onMakeDecision = () => {
  const randomNum = Math.floor(Math.random() * app.options.length);
  const option = app.options[randomNum];
  alert(option);
}

const renderApp = () => {
  const template = (
    <div>
      <h1>{app.title}</h1>
      {app.subtitle && <p>{app.subtitle}</p>}
      <p>{app.options.length > 0 ? 'Here are your options' : 'no options'}</p>
      <button disabled={app.options.length === 0} onClick={onMakeDecision}>What Should I do ?</button>
      <button onClick={remove}>Remove All</button>

      <ol>
        {
          app.options.map( (list) => <li key={list}>{list}</li> )
        }
      </ol>
      <form onSubmit={onFormSubmit}>
        <input type="text" name="option" />
        <button>Add Option</button>
      </form>
    </div>
  );
  ReactDOM.render(template, appRoot);
}

const appRoot = document.getElementById('app');

renderApp();


// if statement
// ternary operator
// logical and operator

// const user = {
//   name: 'Shofwan',
//   age: 22,
//   location: 'Jakarta'
// }
//
// function getLocation(location){
//   if (location) {
//     return <p>Location: {location}</p>;
//   }
// }
//
// const templateTwo = (
//   <div>
//     <h1>{user.name ? user.name : 'anonymous'}</h1>
//     {(user.age && user.age >= 18) && <p>Age: {user.age}</p>}
//     {getLocation(user.location)}
//   </div>
// );
