var nameVar = 'Shofwan';
nameVar = 'Hanif';
console.log('nameVar',nameVar);

let nameLet = 'Oneshof';
nameLet = 'Twoshof';
console.log('namelet',nameLet);

const nameConst = 'shofconst';
console.log('nameConst', nameConst);

// Block scoping

const fullname = 'Shofwan Aditia';
let firstname;
if (fullname) {
  firstname = fullname.split(' ')[0];
  console.log(firstname);
}

console.log(firstname);
