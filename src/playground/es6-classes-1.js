
class Person {
    constructor(name = 'Anonymous',  age = 0) {
        this.name = name;
        this.age = age;
    }
    getGretting() {       
        return `Hi. I am ${this.name}!`;
    }

    getDescription(){
        return `${this.name} is ${this.age} year(s) old.`;
    }  
}

class Student extends Person {
    constructor(name, age, major) {
        super(name, age);
        this.major = major;
    }
    hasMajor(){
        return !!this.major;
    }
    getDescription(){
        let description = super.getDescription();

        if(this.hasMajor()){
            description += ` Their major is ${this.major}.`;
        }
        return description;
    }
}

//CHALLENGE 2
class Traveler extends Person{
    constructor(name, homeLocation){
        super(name);
        this.homeLocation = homeLocation;
    }
    hasHomeLocation(){
        return !!this.homeLocation;
    }
    getGretting(){
        let gretting = super.getGretting();

        if(this.hasHomeLocation()){
            gretting += `I'm visiting from ${this.homeLocation}.`;
        }
        return gretting;
    }
}

const me = new Student('Shofwan Hanif', 22);
console.log(me.getDescription());

const test = new Student('Titi Aditia', 23, 'computer science');
console.log(test.getDescription());

const travel = new Traveler('Hanif','Samarinda');
console.log(travel.getGretting());

const travels = new Traveler('Hanif');
console.log(travels.getGretting());
//CHALLENGE 1

// class People{
//     constructor(name, age = 0){
//         this.name = name;
//         this.age = age;
//     }

//     getDescription(){
//         return `${this.name} is ${this.age} year(s) old.`;
//     }    
// }

// const test = new People('Titi Aditia', 23);
// console.log(test.getDescription());