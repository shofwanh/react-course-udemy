//VERSI VIDEO
let visibility = false;

const toggleVisibility = () => {
  visibility = !visibility;
  render();
}

const render = () => {
  const jsx = (
    <div>
      <h1>Visibility Toggle</h1>
      <button onClick={toggleVisibility}>
        {visibility ? 'Hide details' : 'Show details'}
      </button>
      {visibility && (
        <div>
          <p>Hey. These are some details you can now see!</p>
        </div>
      )}
    </div>
  )

  ReactDOM.render(jsx, document.getElementById('app'));
}

render();


//VERSI SHOFWAN
/*
const app = {
  title: 'Visibility Toggle',
  details: []
}

const showDetails = (e) => {
  const detail = 'This is your details';
  if (app.details == 0){
    app.details.push(detail);
  }else{
    app.details = [];
  }
  renderApp();
}

const appRoot = document.getElementById('app');
const renderApp = () => {
  const template = (
    <div>
      <h1>{app.title}</h1>
      <button onClick={showDetails}>{app.details == 0 ? 'Show Details' : 'Hide Details'}</button>
      <p>{app.details}</p>
    </div>
  )
  ReactDOM.render(template, appRoot);
}

renderApp();
*/
